// khai báo thư viện express 
const express = require("express");
const { courseRouter } = require("./app/routes/courseRoutes");
const { reviewRounter } = require("./app/routes/reviewRouter");

// khai báo app 
const app = express();

//khai báo port
const port = 8000;
//hàm middleware cho get "/"
app.use((req,res,next) => {
    console.log( new Date());

    next();
})
//hàm middleware cho post "/"
app.post('/' , (req, res,next) => {
    console.log(req.method);
    next();
})

const methodGetMiddleware = (req,res,next) =>{
    console.log(req.method);
    next();
}
//khai báo app cho get
app.get("/", (req,res) => {
    let  today = new Date();
    console.log("middleware cho get");
    res.status(200).json({
        message: `hoomnay là ngày ${today.getDate()} tháng ${today.getMonth() +1} năm ${today.getFullYear()}`
    })
},methodGetMiddleware);


app.post("/", (req,res) => {
    let  today = new Date();
    console.log(`post method ngày ${today.getDate()} tháng ${today.getMonth() +1} năm ${today.getFullYear()}`);
    res.status(200).json({
        message: `POST method`
    })
})

//chạy thử courserounter
app.use('/', courseRouter);

//chạy thử reviewRounter
app.use('/',reviewRounter);
//khỡi động app
app.listen(port, () => {
    console.log(`App listen on port  ${port}`)
})